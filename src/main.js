import { createApp } from "vue";
import "./styles.css";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import hljs from 'highlight.js';
import 'highlight.js/styles/base16/darcula.css';
//import { globalShortcut, window } from '@tauri-apps/api';

const app = createApp(App);

app.directive('highlight', {
    deep: true,
    beforeMount(el, binding) {
        const targets = el.querySelectorAll('code');
        targets.forEach((target) => {
            if (binding.value) {
                target.textContent = binding.value;
            }
            hljs.highlightElement(target);
        });
    },
    updated(el, binding) {
        const targets = el.querySelectorAll('code');
        targets.forEach((target) => {
            if (binding.value) {
                target.textContent = binding.value;
            }
            hljs.highlightElement(target);
        });
    }
});

app.use(vuetify).mount("#app");

// app.use(vuetify).mount('#app').$nextTick(async () => {
//     const isRegistered = await globalShortcut.isRegistered('Ctrl+Shift+I');
//     if (!isRegistered) {
//         await globalShortcut.register('Ctrl+Shift+I', async () => {
//             const win = window.appWindow;
//             const isVisible = await win.isVisible();
//             if (isVisible) {
//                 await win.hide();
//             } else {
//                 await win.show();
//                 await win.setFocus();
//             }
//         });
//     }
// });
//
// // Unregister the shortcut before window unload (optional cleanup)
// window.addEventListener('beforeunload', async () => {
//     await globalShortcut.unregister('Ctrl+Shift+I');
// });


