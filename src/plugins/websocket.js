import WebSocket from "@tauri-apps/plugin-websocket";
import store from "/src/store/store.js";

export default class {
    constructor() {
        this.connection = null;
    }

    async connect() {
        const protocol = 'ws'
        const hostname = import.meta.env.VITE_PUSHER_HOST;
        const port = import.meta.env.VITE_PUSHER_PORT;
        const key = import.meta.env.VITE_PUSHER_APP_KEY;
        this.connection = await WebSocket.connect(`${protocol}://${hostname}:${port}/app/${key}`);

        this.connection.send(JSON.stringify({
            event: 'pusher:subscribe',
            data: {
                auth: '',
                channel: 'app'
            }
        }));
    }

    listen() {
        try {
            this.connection.addListener(message => {
                const response = JSON.parse(message.data)
                const data = JSON.parse(response.data)
                if (typeof data !== 'undefined') {
                    switch (data.message.status) {
                        case 'in_progress':
                            store.dispatch('addDot');
                            break;
                        case 'error':
                        case 'completed':
                            store.dispatch('setAssistantMessage', data.message);
                    }
                }
            })
        } catch (error) {
            store.dispatch('setAssistantMessage', error);
        }
    }

    async send() {
        this.connection.send(JSON.stringify({data: {auth: "", channel: "app"}}))
    }

    disconnect() {
        this.connection.disconnect();
    }
}