import store from "/src/store/store.js";
import {readText, writeText} from "@tauri-apps/plugin-clipboard-manager";
import {isRegistered, unregister} from "@tauri-apps/plugin-global-shortcut";
const createAction = async (shortcut) => {
	return async () => {
		const clipboardText = (await readText())?.toString()?.trim();
		await store.dispatch('setAction', shortcut.type)
		await store.dispatch('setUserMessage', clipboardText)
		await store.dispatch('chat');
		await writeText(store.getters.getMessages[store.getters.getMessages.length - 1].content);
	}
}

export const registerKeystroke = async (shortcut) => {
	const action = await createAction(shortcut);
	const alreadyRegistered = await isRegistered(shortcut.shortcut);
	if (!alreadyRegistered) {
		await register(shortcut.shortcut, action);
	}
}

export const unregisterKeystroke = async (shortcut) => {
	await unregister(shortcut.shortcut);
}
