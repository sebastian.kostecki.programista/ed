import {createStore} from 'vuex'
import ChatModule from "./ChatModule.js";
import ActionsModule from "./ActionsModule.js";

const store = createStore({
	modules: {
		ChatModule,
		ActionsModule
	},
})

export default store