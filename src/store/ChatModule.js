import MyApi from "../../../Ed/src/apis/MyAPI.js";

const ChatModule = {
	state() {
		return {
			messages: [],
			action: "",
			thread: null,
			var: null
		}
	},
	mutations: {
		setAction(state, payload) {
			state.action = payload;
			state.thread = null;
		},
		addMessage(state, payload) {
			state.messages.push(payload);
		},
		addResponseToLastMessage(state, payload) {
			let messages = state.messages;
			let lastMessage = messages[messages.length - 1];
			lastMessage.content = payload;
		},
		setThread(state, payload) {
			state.thread = payload;
		},
		clearMessages(state) {
			state.messages = [];
		},
		addDot(state) {
			let messages = state.messages;
			let lastMessage = messages[messages.length - 1];
			lastMessage.content = lastMessage.content + '.';
		}
	},
	actions: {
		async chat(context, message) {
			context.commit('addMessage', {role: 'assistant', content: ""})
			const query = {
				query: context.getters.getLastUserMessage.content,
				action: context.state.action,
				thread: context.state.thread
			}
			const response = await MyApi.chat(query)
			context.commit('addResponseToLastMessage', response.message)
			context.commit('setThread', response.thread);
		},
		setAssistantMessage(context, payload) {
			context.commit('addResponseToLastMessage', payload.message || payload.text)
		},
		setUserMessage(context, payload) {
			const message = {
				role: 'user',
				content: payload
			}
			context.commit('addMessage', message);
		},
		addDot(context) {
			context.commit('addDot');
		},
		setAction(context, action) {
			context.commit('setAction', action)
		},
		clearAction(context) {
			context.commit('setAction', '')
		},
		clear(context) {
			context.commit('setThread', 0);
			context.commit('clearMessages');
		}
	},
	getters: {
		getMessages(state) {
			return state.messages;
		},
		getLastUserMessage(state) {
			const userMessages = state.messages.filter(function (message) {
				return message.role === 'user';
			})
			return userMessages[userMessages.length - 1];
		},
		getActiveAction(state) {
			return state.action;
		}
	}
}

export default ChatModule;