import MyApi from "../../../Ed/src/apis/MyAPI.js";

const ActionsModule = {
	state() {
		return {
			actions: [],
			models: []
		}
	},
	mutations: {
		createAction(state, payload) {
			state.actions.push(payload);
		},
		deleteAction(state, payload) {
			const actionId = payload.id;
			const actionToDeleteIndex = state.actions.findIndex((obj) => obj.id === actionId);

			if (actionToDeleteIndex > -1) {
				state.actions.splice(actionToDeleteIndex, 1);
			}
		},
		setActions(state, payload) {
			state.actions = payload;
		},
		setModels(state, payload) {
			state.models = payload;
		}
	},
	actions: {
		createAction(context, payload) {
			context.commit('createAction', payload)
		},
		async deleteAction(context, payload) {
			await MyApi.deleteAction(payload);
			context.commit('deleteAction', payload);
		},
		async getActions(context) {
			const response = await MyApi.actions();
			context.commit('setActions', response);
		},
		async getModels(context) {
			const response = await MyApi.models();
			context.commit('setModels', response);
		}
	},
	getters: {
		getActions(state) {
			return state.actions;
		},
		getModels(state) {
			return state.models;
		}
	}
}
export default ActionsModule;