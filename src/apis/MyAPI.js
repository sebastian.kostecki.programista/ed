import Api from "./Api.js";

export default {
	async actions() {
		return await Api.get('/assistant/actions')
			.then(response => {
				return response.data.data;
			})
	},
	async models() {
		return await Api.get('/assistant/models')
			.then(response => {
				return response.data.data;
			})
	},
	async createAction(action) {
		return await Api.post(`/assistant/action/create`, action)
			.then(response => {
				return response.data.data;
			})
	},
	async updateAction(action) {
		return await Api.put(`/assistant/action/${action.id}/update`, action)
			.then(response => {
				return response.data.data;
			})
	},
	async deleteAction(action) {
		return await Api.delete(`/assistant/action/${action.id}`)
			.then(response => {
				return response.data;
			})
	},
	async chat(query) {
		return await Api.post('/assistant', query)
			.then(response => {
				return response.data;
			})
			.catch((error) => {
				return `Failed: ${error.message}`
			})
	}
}