import axios from "axios";

const url = import.meta.env.VITE_BASE_URL;
const apiToken = import.meta.env.VITE_API_TOKEN;

const Api = axios.create({
	baseURL: url,
	headers: {
		Authorization: `Bearer ${apiToken}`
	}
});

export default Api;